import React from "react"

function Welcome(props){
    return <h1>Halo, {props.name}</h1>;
}

export default Welcome;