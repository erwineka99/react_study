import React from "react"
import { BrowserRouter,Routes,Route } from 'react-router-dom'
import Home from "./Home"
import { useEffect } from "react"
import axios from 'axios'
import { useState } from "react"

const Profil = () =>{
  return <p>Ini Profil</p>
}



const App = () =>{
  const [data,setData] = useState(null)
  const [input, setInput] = useState({
    name :""
  })
  const [fetchStatus,setFetchStatus]=useState(true)

  const handleInput = (event) =>{
    let name = event.target.name
    let value = event.target.value
    if(name ==="name"){
      setInput({...input,name:value})
    }
  }

  const handleSubmit = (event) =>{
    event.preventDefault();
    let{
      name
    } =input

    axios.post(' https://backendexample.sanbercloud.com/api/contestants',{name})
    .then((res)=>{
      console.log(res)
      setFetchStatus(true)
    })

    setInput({
      name:""
    })
  }

  const handleDelete= (event) =>{
    let idData = parseInt(event.target.value)

    axios.delete(`https://backendexample.sanbercloud.com/api/contestants/${idData}`)
    .then((res)=>{
      setFetchStatus(true)
    })
  }

  useEffect(()=>{
    if(fetchStatus===true){
        axios.get("https://backendexample.sanbercloud.com/api/contestants")
        .then((res)=>{
          // console.log(res.data)
          setData([...res.data])
        })
        .catch((error)=>{

        })
        setFetchStatus(false)
    }
    
  },[fetchStatus,setFetchStatus])
  //console.log(data)
  return (
    <>
    <div>
        <ul>
          {data !==null && data.map((res)=>{
            return (
              <>
                <li>{res.name} | &nbsp; <button>edit</button><button onClick={handleDelete} value={res.id}>delete</button></li>
              </>
            )
          })}
        </ul>

        <p>FORM DATA</p>
        <form onSubmit={handleSubmit}>
          <span>Nama : </span>
          <input onChange={handleInput} value={input.name} type="text" name='name' />
          <input type={'submit'}/>
        </form> 
      </div>
    </>
    
  );
  
}

export default App;
